﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Editar Perfil/EditarPerfil.aspx.cs" Inherits="Views_Editar_Perfil_EditarPerfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <br />
                               <div class="form-group">
                                <label for="exampleInputEmail1">Nombre</label>
                                  <asp:TextBox ID="TX_Nombre" runat="server"></asp:TextBox>
                              
                              </div>
                              <asp:RegularExpressionValidator ID="RV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*Digite solo letras por favor" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Editar"></asp:RegularExpressionValidator>
                              <asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                              <br />

                                    <div class="form-group">
                                <label for="exampleInputEmail1">Apellido</label>
                               <asp:TextBox ID="TX_Apellido" runat="server"></asp:TextBox>
                               
                              </div>
                                  <asp:RegularExpressionValidator ID="RV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*Digite solo letras por favor" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Editar"></asp:RegularExpressionValidator>
                              <asp:RequiredFieldValidator ID="RFV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                                  <br />
                               <div class="form-group">
                                <label for="exampleInputEmail1">Correo Electronico</label>
                                   <asp:Label ID="LA_Correo" runat="server" Text="Label"></asp:Label>
                              </div>
                            

                                  <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña</label>
                                 <asp:TextBox ID="TX_Clave" TextMode="Password"  runat="server"></asp:TextBox>
                              </div>
                              <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Editar"></asp:RegularExpressionValidator>
                              <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                          <br />
                                
                                  <div class="form-group">
                                <label for="exampleInputPassword1">RepetirContraseña</label>
                                       <asp:TextBox ID="TX_Repetir_Clave" TextMode="Password"  runat="server"></asp:TextBox>
                              </div>

                                      <asp:CompareValidator ID="CV_Repetir_Clave_Registro" runat="server" ControlToCompare="TX_Clave" ControlToValidate="TX_Repetir_Clave" Display="Dynamic" ErrorMessage="*La contraseña no coincide." ForeColor="Red" ValidationGroup="VG_Editar"></asp:CompareValidator>
                              <asp:RequiredFieldValidator ID="RFV_Repetir_Clave" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>

                                      <br />
                                 <asp:FileUpload ID="FU_Imagen_Usuario" runat="server" />
                               <br />      
                           <br />      
                
                                <asp:Button ID="BTN_Editar_Perfil" runat="server" class="btn btn-primary" Text="Editar Perfil"  ValidationGroup="VG_Editar" OnClick="BTN_Editar_Perfil_Click" />

                               <asp:Image ID="IM_Perfil_Actual" runat="server" />

                  
                                 
</asp:Content>

