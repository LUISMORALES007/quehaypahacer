﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/RecuperarContraseña.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_RecuperarContraseña" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Recuperar Contraseña</title>
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header text-center">
                        <h5>Recuperar Contraseña</h5>
                    </div>

                    <div class="card-body">

                        <form id="form1" runat="server">

                            <div class="form-group row">
                                <label for="inputPassword" class="col-md-4 col-form-label text-md-right">Digite la Nueva Contraseña</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="TX_Clave" TextMode="Password" runat="server"></asp:TextBox>

                                    <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Recuperar"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Recuperar"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-md-4 col-form-label text-md-right">RepetirContraseña</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="TX_Repetir_Clave" TextMode="Password" runat="server"></asp:TextBox>

                                    <asp:CompareValidator ID="CV_Repetir_Clave_Registro" runat="server" ControlToCompare="TX_Clave" ControlToValidate="TX_Repetir_Clave" Display="Dynamic" ErrorMessage="*La contraseña no coincide." ForeColor="Red" ValidationGroup="VG_Recuperar"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="RFV_Repetir_Clave" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Recuperar"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <asp:Button ID="BTN_Recuperar" runat="server" class="btn btn-primary" Text="Recuperar Contraseña" ValidationGroup="VG_Recuperar" OnClick="BTN_Recuperar_Click" />
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="../../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
