﻿<%@ Page Title="Registro de usuario" Language="C#"  AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/RegistroUsuario.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_RegistroUsuario" %>


<head>
    

    <!-- FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Template Main CSS File -->
    <link href="../../assets/css/registro.css" rel="stylesheet">

    </head>


<form  runat="server">
     <div class="main-w3layouts wrapper">
         <div>
             <img class="img-fluid rounded float-left" src="../../assets/img/logo-melo.png" alt="Responsive image" > 
             <h1>Registro de nuevo usuario </h1>
         </div>
         <br />
         <br />
         <br />
         <br />
         <div class="main-agileinfo">
                <div class="agileits-top">
                    
                    
                        <div class="form-group nom">
                                
                                <asp:TextBox ID="TX_Nombre" placeholder="Nombre" runat="server"></asp:TextBox>
                              
                                  
                              
                                  <asp:RegularExpressionValidator ID="RV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*Digite solo letras" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                  <asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                             
                        </div>
                    <br />

                        <div class="form-group ape">
                              <asp:TextBox ID="TX_Apellido" placeholder="Apellido" runat="server"></asp:TextBox>
                              <asp:RegularExpressionValidator ID="RV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*Digite solo letras" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                              <asp:RequiredFieldValidator ID="RFV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                 
                        </div>
                    <br />
                         <div class="form-group">
                              <asp:TextBox ID="TX_Correo" TextMode="Email"  placeholder="Correo electrónico" runat="server"></asp:TextBox>
                              <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo válido." ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                              <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                  
                           </div>
                              
                    <br />

                      <div class="form-group">
                            <asp:TextBox ID="TX_Clave" TextMode="Password" placeholder="Contraseña" runat="server"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*La contraseña debe tener minimo 1 mayúscula, 1 minúscula, 1 caracter especial y 6 o más carácteres" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@|#|.|,|_|-|\|!| |#|$|%|&|/|(|)|?|Ñ|ñ]).{6,20}$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                            
                       </div>
                             
                        <div class="form-group">
                                <asp:TextBox ID="TX_Repetir_Clave" TextMode="Password"  placeholder="Repetir contraseña" runat="server"></asp:TextBox>
                                <asp:CompareValidator ID="CV_Repetir_Clave_Registro" runat="server" ControlToCompare="TX_Clave" ControlToValidate="TX_Repetir_Clave" Display="Dynamic" ErrorMessage="*La contraseña no coincide." ForeColor="Red" ValidationGroup="VG_Registro"></asp:CompareValidator>
                                 <asp:RequiredFieldValidator ID="RFV_Repetir_Clave" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                        </div>
                    <br />
                         <div>
                                 <label class="I_image" for="exampleInputImage">Inserte una imagen</label>&nbsp;<br />
                                 <asp:FileUpload ID="FU_Imagen_Usuario" runat="server" />
                                 &nbsp;&nbsp;&nbsp;
                                 <asp:RequiredFieldValidator ID="RFV_Imagen_Registro" runat="server" ControlToValidate="FU_Imagen_Usuario" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                              
                     <br />
                     <br />
                              
                         </div>
                                    
                         <div class="form-group T_terminos">
                                <label for="TxtTerminos">Términos y condiciones de privacidad</label>
                                   <textarea class="form-control" id="TxtTerminos" name="S1" rows="4">   Conforme a las disposiciones contenidas en la Ley Estatutaria 1581 de 2012 y al Decreto 1377 de 2013 que desarrolla parcialmente la Ley de Habeas Data, QUE HAY PA HACER S.A., considerada como responsable del tratamiento de datos personales, desarrolla la presente política de privacidad y de protección de datos personales, en cuanto a su recolección, almacenamiento y administración.

Lo anterior, en armonía con el cumplimiento de las disposiciones especiales que hacen referencia al tratamiento de datos personales de naturaleza financiera, crediticia, comercial y de servicios previstas en la Ley 1266 de 2008, así como en las normas que la reglamenten y modifiquen, que cobijan el origen, mantenimiento, administración y extinción de obligaciones derivadas de relaciones de tal naturaleza.

QUE HAY PA HACER S.A., sin perjuicio de lo establecido en los formatos que pueden hallarse en su página, respeta la privacidad de sus usuarios, toda vez que la información que se solicita y el visitante suministra voluntariamente, autoriza expresamente a QUE HAY PA HACER S.A., al tratamiento de sus datos personales, en el entendido que tal información hará parte de un archivo y/o base de datos, cumpliendo con los principios de finalidad y libertad, puesto que aquellos datos son pertinentes y adecuados para la finalidad para la cual son recolectados o requeridos conforme a la normatividad vigente.

QUE HAY PA HACER S.A., conservará los archivos o bases de datos que contengan datos personales por el período que la normatividad vigente así se lo exija o lo permita, la vigencia de las bases de datos estará atada al ejercicio del objeto social de la Entidad, adicionalmente no transmitirá ni utilizará la información suministrada excediendo los límites establecidos por la ley, exceptuando casos en los que el titular autorice aun tercero o cuando dicha información sea requerida por una autoridad competente.

Si dentro de los 30 días siguientes, a la notificación de esta Política De Privacidad Y Protección De Datos Personales, el cliente no manifiesta a QUE HAY PA HACER S.A., información en contrario, se entenderá la autorización tacita para el manejo de la información del usuario, de conformidad al numeral 4 del Artículo 10 del decreto 1377 de 2013.

El usuario podrá solicitar la modificación, actualización o bloqueo de sus datos personales en cualquier momento, mediante el envío de una carta autenticada, al domicilio legal de la entidad o al correo electrónico quehaypahacer@gmail.com
                                  </textarea>
                         </div>
                     <br />
                     
                        <div class="wthree-text ">
						<label class="anim">
							<input type="checkbox" class="checkbox" ID="CHK_Terminos" required="">
							<span>Acepto los términos y condiciones</span>
						</label>
						<div class="clear"> </div>
					</div>
                        
                       
                      <br />      
                      
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="BTN_Registro" runat="server" class="btn btn-success" Text="Registrarse" OnClick="BTN_Registro_Click" ValidationGroup="VG_Registro" />
                   
             
             <p>¿Tienes una cuenta? <a href="#"> Ingresar</a></p>
          </div>
             
         </div>
    <div class="colorlibcopy-agile">
			<p>Copyright  © 2020 QUEHAYPAHACER Todos los derechos reservados. | Design by <a href="../Principal/Default.aspx" target="_blank">Quehaypa'hacer</a></p>
      <p>Desarrollado por: Luis Morales, Nicolás Mantilla y Raúl Mogollón.</p>
      <p>UNIVERSIDAD DE CUNDINAMARCA</p>
    </div>
		
    </div>
<!-- //main -->
</form>
    



