﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/GenerarToken.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_GenerarToken" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Genear Token</title>
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header text-center">
                        <h5>Generar Token</h5>
                    </div>

                    <div class="card-body">

                        <form id="form1" runat="server">

                            <div class="form-group row">

                                <label for="inputEmail" class="col-md-4 col-form-label text-md-right">Correo Electronico</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="TX_Correo" TextMode="Email" runat="server"></asp:TextBox>

                                    <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo." ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_Token"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Token"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">

                                    <asp:Button ID="BTN_Enviar_Token" runat="server" class="btn btn-warning" Text="Recuperar" ValidationGroup="VG_Token" OnClick="BTN_Enviar_Token_Click" />
                                    <br />
                                    <asp:Label ID="L_Mensaje" runat="server"></asp:Label>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="../../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
