﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/IniciarSesion.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_IniciarSesion" %>

<link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header text-center">
                    <h5>Iniciar Sesion</h5>
                </div>

                <div class="card-body">
                    <form runat="server">

                        <div class="form-group row">

                            <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                <h6>Correo Electronico</h6>
                            </label>
                            <div class="col-md-6">
                                <asp:TextBox ID="TX_Correo" TextMode="Email" runat="server" placeholder="Correo Electronico"></asp:TextBox>


                                <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo." ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_Inicio_Sesion"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Inicio_Sesion"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="inputPassword" class="col-md-4 col-form-label text-md-right">
                                <h6>Contraseña</h6>
                            </label>

                            <div class="col-md-6">
                                <asp:TextBox ID="TX_Clave" TextMode="Password" runat="server" placeholder="Contraseña"></asp:TextBox>

                                <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Inicio_Sesion"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Inicio_Sesion"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">

                                <asp:Button ID="BTN_Inicio_Sesion" runat="server" class="btn btn-warning" Text="Iniciar Sesion" ValidationGroup="VG_Inicio_Sesion" OnClick="BTN_Inicio_Sesion_Click" />
                                <asp:button runat="server" text="Regresar" ID="B_Regresar" class="btn btn-warning" OnClick="B_Regresar_Click" />
                                <br />
                                <asp:linkbutton runat="server" ID="LB_Recuperar" OnClick="LB_Recuperar_Click" class="btn btn-link">Recuperar Contraseña</asp:linkbutton>
                                <br />
                                <asp:Label ID="LA_Mensaje" runat="server" ForeColor="Red"></asp:Label>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="../../assets/vendor/jquery/jquery.min.js"></script>
<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

