﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class Views_Inicio_de_Sesion_y_Registro_GenerarToken : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void BTN_Enviar_Token_Click(object sender, EventArgs e)
    {
        ///falta validar si el correo es el mirmo al q el usuario digito
        DAOUsuario dao = new DAOUsuario();
        System.Data.DataTable validez = dao.generarToken(TX_Correo.Text);

        if (int.Parse(validez.Rows[0]["id"].ToString()) > 0)
        {
            EUserToken token = new EUserToken();
            token.Id = int.Parse(validez.Rows[0]["id"].ToString());
            token.Nombre = validez.Rows[0]["nombre"].ToString();
            token.Apellido= validez.Rows[0]["apellido"].ToString();
            token.Correo = validez.Rows[0]["correo"].ToString();
            token.Estado = int.Parse(validez.Rows[0]["estado"].ToString());
            token.Fecha = DateTime.Now.ToFileTimeUtc();

            String userToken = encriptar(JsonConvert.SerializeObject(token));
            
           dao.almacenarToken(userToken, token.Id);

           Correo correo = new Correo();

              String mensaje = "su link de acceso es :   " + "http://localhost:51101/Views/Inicio%20de%20Sesion%20y%20Registro/RecuperarContraseña.aspx?" + userToken;
              correo.enviarCorreo(token.Correo, userToken, mensaje);

             L_Mensaje.Text = "El token de recuperacion de contraseña  ha sido enviado a su correo";
        }
        else if (int.Parse(validez.Rows[0]["id"].ToString()) == -2)
        {
            L_Mensaje.Text = "Ya extsite un token, por favor verifique su correo.";
        }
        else
        {
            L_Mensaje.Text = "El usurio digitado no existe";
        }



    }

    private string encriptar(string input)
    {
        SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();

        byte[] inputBytes = Encoding.UTF8.GetBytes(input);
        byte[] hashedBytes = provider.ComputeHash(inputBytes);

        StringBuilder output = new StringBuilder();

        for (int i = 0; i < hashedBytes.Length; i++)
            output.Append(hashedBytes[i].ToString("x2").ToLower());

        return output.ToString();
    }
    
}