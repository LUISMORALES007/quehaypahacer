﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Inicio_de_Sesion_y_Registro_IniciarSesion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LA_Mensaje.Text = " ";
    }

    protected void BTN_Inicio_Sesion_Click(object sender, EventArgs e)
    {

        
        Session["usuario_session"] = Session.SessionID;

                EUsuario iniciarsesion = new EUsuario();

                iniciarsesion.Correo = TX_Correo.Text;
                iniciarsesion.Clave = TX_Clave.Text;

                DataTable datosTabla = new DAOUsuario().validacionInicioSesion( iniciarsesion);

        if (datosTabla.Rows.Count != 0)
        {
            EUsuario datosUsuario = new EUsuario();
            MAC datosConexion = new MAC();

            DataTable datosUsuarioTabla = new DAOUsuario().datosUsuario(datosTabla.Rows[0]["correo_usuario"].ToString());

            datosUsuario.Codigo_usuario = long.Parse(datosUsuarioTabla.Rows[0]["id"].ToString());
            datosUsuario.Nombre= datosUsuarioTabla.Rows[0]["nombre"].ToString();
            datosUsuario.Apellido= datosUsuarioTabla.Rows[0]["apellido"].ToString();
            datosUsuario.Correo = datosUsuarioTabla.Rows[0]["correo"].ToString();
            datosUsuario.Url_imagen= datosUsuarioTabla.Rows[0]["url_imagen"].ToString();
            datosUsuario.Rol_usuario = int.Parse(datosUsuarioTabla.Rows[0]["id_rol"].ToString());
            datosUsuario.Habilitado = Boolean.Parse(datosUsuarioTabla.Rows[0]["habilitado"].ToString());
            datosUsuario.Session = Session.SessionID;
            datosUsuario.Ip = datosConexion.ip();
            datosUsuario.Mac = datosConexion.mac();
        

            if (datosUsuario.Habilitado.Equals(true)) {

                Session["usuario_datos"] = datosUsuario;
                Session["codigo_usuario"] = datosUsuario.Codigo_usuario;

                new DAOUsuario().guardarSesion(datosUsuario);

         
                 Response.Redirect("~\\Views\\Principal\\Default.aspx");

                    
             }else{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('El Usuario esta deshabilitado')", true);
                LA_Mensaje.Text = "El Usuario Esta Deshabilitado";
               
            }
        
        }
        else
        {
          // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Correo o Clave Incorrectos')", true);
            LA_Mensaje.Text = "El Correo o la Contraseña ingresados son incorrectos, intente nuevamente";


        }


        TX_Correo.Text = " ";
    }

    protected void LB_Recuperar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\GenerarToken.aspx");
    }

    protected void B_Regresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\Default.aspx");
    }
}