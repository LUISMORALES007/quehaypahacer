﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Inicio_de_Sesion_y_Registro_RegistroUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void BTN_Registro_Click(object sender, EventArgs e)
    {
        ClientScriptManager cm = this.ClientScript;
        Session["usuario_session"] = Session.SessionID;

           EUsuario registrarusuario = new EUsuario();
          FileUpload FU_IDocumento = FU_Imagen_Usuario;

                    if (FU_IDocumento.PostedFile.FileName != "" && !(System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".jpg") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".jpng") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".png") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".bmp")))
                    {
                        cm.RegisterClientScriptBlock(this.GetType(), "", "<script type='text/javascript'>alert('inserte solo imagenes');</script>");
                        return;
                    }
                    else
                    {
                        registrarusuario.Url_imagen = "~\\Imagenes\\Usuarios\\" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);
                        FU_IDocumento.PostedFile.SaveAs(Server.MapPath(registrarusuario.Url_imagen));
                    }


                    registrarusuario.Nombre = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Nombre.Text).ToLower());
                    registrarusuario.Apellido = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Apellido.Text).ToLower());
                    registrarusuario.Correo = (TX_Correo.Text).ToLower();
                    registrarusuario.Clave = TX_Clave.Text;
                    registrarusuario.Session = (String)Session["usuario_session"];


                                if (!new DAOUsuario().validarExistenciaCorreo(registrarusuario.Correo))
                                {

                                              new DAOUsuario().registro(registrarusuario);
                                              cm.RegisterClientScriptBlock(this.GetType(), "", "<script type='text/javascript'>alert('Se ha registrado correctamente.');</script>");
                                              Response.Redirect("~\\Views\\Principal\\Default.aspx");
                                }
                                else {

                                     cm.RegisterClientScriptBlock(this.GetType(), "", "<script type='text/javascript'>alert('El correo ya ha sido registrado.');</script>");
                                     Response.Redirect("~\\Views\\Principal\\Default.aspx");
                                   }

                                TX_Nombre.Text = " ";
                                TX_Apellido.Text = " ";
                                 TX_Correo.Text = " ";


        }
}