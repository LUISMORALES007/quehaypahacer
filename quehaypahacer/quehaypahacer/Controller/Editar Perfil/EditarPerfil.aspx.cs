﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Views_Editar_Perfil_EditarPerfil : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TX_Nombre.Text = ((EUsuario)(Session["usuario_datos"])).Nombre;
            TX_Apellido.Text = ((EUsuario)(Session["usuario_datos"])).Apellido;
            LA_Correo.Text = ((EUsuario)(Session["usuario_datos"])).Correo;
            IM_Perfil_Actual.ImageUrl = ((EUsuario)(Session["usuario_datos"])).Url_imagen;
        }
    }

    protected void BTN_Editar_Perfil_Click(object sender, EventArgs e)
    {
        if (TX_Clave.Text.Equals(TX_Repetir_Clave.Text)) {


            DAOEditarUsuario editar = new DAOEditarUsuario();
            EUsuario datosIntroducidos = new EUsuario();

            FileUpload FU_IDocumento = FU_Imagen_Usuario;
            ClientScriptManager cm = this.ClientScript;
            long numeroImagen = 0;

            if (FU_IDocumento.PostedFile.FileName != "")
            {
                String extensionImagen = System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).ToLower();
                if (!(extensionImagen.Equals(".jpg") || extensionImagen.Equals(".jpng") || extensionImagen.Equals(".png") || extensionImagen.Equals(".bmp") || extensionImagen.Equals(".jpeg")))
                {
                    cm.RegisterClientScriptBlock(this.GetType(), "", "<script type='text/javascript'>alert('Solo Imagenes');</script>");
                    return;
                }
                else
                {
                    datosIntroducidos.Url_imagen = "~\\Imagenes\\Usuarios\\" + numeroImagen.ToString() + "_" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);

                    do
                    {
                        numeroImagen++;
                        datosIntroducidos.Url_imagen = "~\\Imagenes\\Usuarios\\" + numeroImagen.ToString() + "_" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);

                    } while (new DAOEditarUsuario().validarExistenciaImagen(datosIntroducidos.Url_imagen));

                    FU_IDocumento.PostedFile.SaveAs(Server.MapPath(datosIntroducidos.Url_imagen));
                    ((EUsuario)(Session["usuario_datos"])).Url_imagen = datosIntroducidos.Url_imagen;
                }
            }
            else
            {
                datosIntroducidos.Url_imagen= ((EUsuario)(Session["usuario_datos"])).Url_imagen;
            }

            datosIntroducidos.Nombre = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Nombre.Text).ToLower());
            datosIntroducidos.Apellido = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Apellido.Text).ToLower());
            datosIntroducidos.Correo = LA_Correo.Text;
            datosIntroducidos.Clave = TX_Clave.Text;
            datosIntroducidos.Session = Session.SessionID;

          

            ((EUsuario)Session["usuario_datos"]).Nombre = datosIntroducidos.Nombre;
            ((EUsuario)Session["usuario_datos"]).Apellido = datosIntroducidos.Apellido;
            ((EUsuario)Session["usuario_datos"]).Correo = datosIntroducidos.Correo;
            ((EUsuario)Session["usuario_datos"]).Clave = datosIntroducidos.Clave;
            ((EUsuario)Session["usuario_datos"]).Session = datosIntroducidos.Session;

            editar.editarUsuario(datosIntroducidos);
            Response.Redirect("~\\Views\\Editar Perfil\\EditarPerfil.aspx");
         
       

        }

    }
}