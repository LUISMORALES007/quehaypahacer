﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Principal_DefaultRegistrados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        switch (((EUsuario)Session["usuario_datos"]).Rol_usuario)
        {
            case 1:

                //modulo administrador del sistema

                LA_Bienvenida.Text = " BIENVENIDO ADMINISTRADOR ";


                ;
                break;

            case 2:

                //usuario Miembro
                LA_Bienvenida.Text = " BIENVENIDO " + ((((EUsuario)Session["usuario_datos"]).Nombre).ToString()).ToUpper();
      
                break;

            default:

                //default

                break;
        }
    }
}