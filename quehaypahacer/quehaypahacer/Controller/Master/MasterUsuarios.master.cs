﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Master_MasterUsuarios : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["usuario_datos"] == null)
        {
            B_Cerrar_Session.Visible = false;
            LB_Menu_Eventos.Visible = false;
            LB_Menu_Administrador.Visible = false;
          
        }
        else if (((EUsuario)Session["usuario_datos"]).Session == null)
        {
            B_Cerrar_Session.Visible = false;
            LB_Menu_Eventos.Visible = false;
            LB_Menu_Administrador.Visible = false;
    
        }
        else {

            switch (((EUsuario)Session["usuario_datos"]).Rol_usuario)
            {
                case 1:

                    //modulo administrador del sistema
                    LB_Menu_Administrador.Visible = true;
                    B_Cerrar_Session.Visible = true;
                    B_Inicio_Sesion.Visible = false;
                    B_Registro.Visible = false;

                    break;

                case 2:

                    //usuario Miembro
                    LB_Menu_Eventos.Visible = true;
                    B_Cerrar_Session.Visible = true;
                    B_Inicio_Sesion.Visible = false;
                    B_Registro.Visible = false;
                    break;

                default:

                    //default

                    break;
            }
        }
    }

    protected void B_Registro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\RegistroUsuario.aspx");
    }

    protected void B_Inicio_Sesion_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\IniciarSesion.aspx");
    }

    protected void B_Cerrar_Session_Click(object sender, EventArgs e)
    {

        Session["usuario_datos"] = null;
        Session["usuario_session"] = null;
        Session["codigo_usuario"] = null;

        DAOUsuario user = new DAOUsuario();
        EUsuario datos = new EUsuario();
        datos.Session = Session.SessionID;
        user.cerrarSesion(datos);

        LB_Menu_Eventos.Visible = false;
        LB_Menu_Administrador.Visible = false;
        B_Cerrar_Session.Visible = false;
        B_Inicio_Sesion.Visible = true;
        B_Registro.Visible = true;
    }

    protected void LB_Menu_Eventos_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\DefaultRegistrados.aspx");
    }

    protected void LB_Menu_Administrador_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\DefaultRegistrados.aspx");
    }
}
