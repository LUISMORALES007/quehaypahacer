﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Master_MasterRegistrados : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
  
        if (Session["usuario_datos"] == null)
        {
            MenuUsuarioMiembro.Visible = false;
            MenuAdministrador.Visible = false;
            B_Cerrar_Session.Visible = false;
            B_Editar_Perfil.Visible = false;
    
        }
        else if (((EUsuario)Session["usuario_datos"]).Session == null)
        {
            MenuUsuarioMiembro.Visible = false;
            MenuAdministrador.Visible = false;
            B_Cerrar_Session.Visible = false;
            B_Editar_Perfil.Visible = false;
          
        }
        else
        {
            MenuUsuarioMiembro.Visible = false;
            MenuAdministrador.Visible = false;
            switch (((EUsuario)Session["usuario_datos"]).Rol_usuario)
            {
                case 1:

                    //modulo administrador del sistema
                    MenuAdministrador.Visible = true;
            


                    ;
                    break;

                case 2:
                
                    //usuario Miembro
             
                    MenuUsuarioMiembro.Visible = true;
                    break;

                default:

                    //default

                    break;
            }
        }
    
        
    }

    protected void B_Cerrar_Session_Click(object sender, EventArgs e)
    {
        Session["usuario_datos"] = null;
        Session["usuario_session"] = null;
        Session["codigo_usuario"] = null;

        DAOUsuario user = new DAOUsuario();
        EUsuario datos = new EUsuario();
        datos.Session = Session.SessionID;
        user.cerrarSesion(datos);

        MenuUsuarioMiembro.Visible = false;
        MenuAdministrador.Visible = false;
       
        Response.Redirect("~\\Views\\Principal\\Default.aspx");
    }

    protected void B_Editar_Perfil_Click(object sender, EventArgs e)
    {

        Response.Redirect("~\\Views\\Editar Perfil\\EditarPerfil.aspx");
    }

    protected void MenuUsuarioMiembro_MenuItemClick(object sender, MenuEventArgs e)
    {

    }



    protected void MenuAdministrador_MenuItemClick(object sender, MenuEventArgs e)
    {
    
    }
}
