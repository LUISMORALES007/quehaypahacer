﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de EUserToken
/// </summary>
public class EUserToken
{
    public EUserToken()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    private long id;
    private String nombre;
    private String apellido;
    private String correo;
    private Int32 estado;
    private long fecha;

    public long Id { get => id; set => id = value; }
    public string Nombre { get => nombre; set => nombre = value; }
    public string Apellido { get => apellido; set => apellido = value; }
    public string Correo { get => correo; set => correo = value; }
    public int Estado { get => estado; set => estado = value; }
    public long Fecha { get => fecha; set => fecha = value; }
}